window.iok_user_action = true
window.iokIsUserActive = true
window.iokonicTimer = null
window.iokOldTimestamp = 0

function addTimerUI() {
    var timer = document.createElement('div')

    timer.id = '#iok_timer'

    timer.style.backgroundColor = 'white'

    timer.style.borderRadius = '20px'
    timer.style.border = '2px solid #0073aa'

    var text = ''

    if (window.iok_tracking.is_logged_in) {
        text = '0.00 min';
    } else {
        text = 'You are not logged in. <br>Log in to earn money by reading news'
        text += '<br> <span> <a target="_blank" href="' + window.iok_tracking.info_url + '">Know more!</a></span>'
        text += '<br> <span><u id="iok_close">close</u></span>'
    }

    timer.innerHTML = text
    timer.style.textAlign = 'center'
    timer.style.fontSize = '100%'
    timer.style.boxShadow = '1px gray'

    timer.style.position = 'fixed'
    timer.style.bottom = '10px'
    timer.style.right = '10px'
    timer.style.padding = '0.5rem 0.5rem'
    timer.style.zIndex = 9999

    document.body.append(timer)
}

function verifyUserLogin() {
    //mark user interaction

    if (window.jQuery && window.iokIsUserActive) {
        jQuery.ajax({
            type: "GET",
            url: ajax_object.ajax_url,
            data: {
                is_logged_in: window.iok_tracking.is_logged_in,
                is_post: window.iok_tracking.is_post,
                post_id: window.iok_tracking.post_id,
                page_url: window.iok_tracking.page_url,
                action: "track_time"
            },
            success: function (data) {
                var timer = document.getElementById('#iok_timer')
                var data = jQuery.parseJSON(data)


                timer.innerHTML = data.total_time + ' min.'
            },
            error: function (errorThrown) {
                console.log(errorThrown)
            }
        });
    }
}

function scheduleTimeTracking() {
    var interval = window.iok_tracking.interval * 1000;
    window.iokonicTimer = setInterval(verifyUserLogin, interval);
}

function sendRedeemRequest() {
    if (window.jQuery) {
        jQuery.ajax({
            type: "GET",
            url: ajax_object.ajax_url,
            data: {
                action: "iok_redeem_request"
            },
            success: function (data) {
                var data = jQuery.parseJSON(data)

                alert(data.message)

                console.log(data)
            },
            error: function (errorThrown) {
                console.log(errorThrown)
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    addTimerUI()

    if (window.iok_tracking.is_logged_in)
        scheduleTimeTracking()

    var closeBtn = document.querySelector('#iok_close')

    if (closeBtn)
        closeBtn.onclick = function () {
            var timer = document.getElementById('#iok_timer')

            timer.style.display = 'none'
        }

    var requestRedeemBtn = document.querySelector('#btn_request_redeem')

    if (requestRedeemBtn) {
        requestRedeemBtn.onclick = sendRedeemRequest
    }

    idle({
        onIdle: function () {
            console.log('It\'s been a long time since you don\'t see me');

            var timer = document.getElementById('#iok_timer')

            window.iokOldTimestamp = timer.innerText

            timer.innerHTML = 'Your timer has been paused due to inactivity'
            window.iokIsUserActive = false
        },
        onActive: function () {

            var timer = document.getElementById('#iok_timer')

            timer.innerHTML = window.iokOldTimestamp

            console.log('you are back');

            window.iokIsUserActive = true
        },
        idle: 60000,
        startAtIdle: true
    }).start()
})
