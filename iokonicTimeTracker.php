<?php
/**
 * @package IokonicTimeTracker
 */
/*
Plugin Name: Iokonic Time Tracker
Plugin URI: http://iokonic.tech
Description: Time tracker for user
Version: 0.0.1
Author: Iokonic Tech
Author URI:  http://iokonic.tech
License: GPLv2 or later
Text Domain: okonictimetracker
 */

//Make sure wordpress is initialised correctly
defined('ABSPATH') or die('Server error');

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('IOKONIC_VERSION', '0.0.1');

//register the plugin
/**
 * Register a style sheet to configure the component which show
 * the total amount of time spend in reading the article/
 */
function iok_enqueue_style()
{
    wp_enqueue_style('mycss', plugins_url('css/mycss.css', __FILE__));
}

//register the stylesheet
add_action('wp_enqueue_scripts', 'iok_enqueue_style');

/**
 * Register a style sheet to configure the component which show
 * the total amount of time spend in reading the article/
 */
function iok_enqueue_script()
{
    wp_enqueue_script('myjs', plugins_url('js/myjs.js', __FILE__));
    wp_enqueue_script('idleVanilla', plugins_url('js/idle.vanilla.js', __FILE__));
    wp_localize_script('myjs', 'ajax_object',
        array('ajax_url' => admin_url('admin-ajax.php')));
}

//register the js script
add_action('wp_enqueue_scripts', 'iok_enqueue_script');

if (!function_exists('iok_activation_hook')) {
    function iok_activation_hook()
    {
        global $wpdb;

        $sql = "CREATE TABLE IF NOT EXISTS wp_iok_tracking(id INTEGER AUTO_INCREMENT PRIMARY KEY,
                    user_id INTEGER, post_id INTEGER, time_spent INTEGER, page_name VARCHAR(255),
                    time_interval INTEGER, paid VARCHAR(10) DEFAULT 'no', created_at TIMESTAMP DEFAULT now())";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);

        $redeem_request = [];

        add_option('iok_tracking_interval', 30, '', 'yes');
        add_option('iok_tracking_per_page', 10, '', 'yes');
        add_option('iok_tracking_track_by', 'post', '', 'yes');
        add_option('iok_tracking_timelimit', '100', '', 'yes');
        add_option('iok_tracking_minimumPayment', '10', '', 'yes');
        add_option('iok_registered_phone', '9876543210', '', 'yes');

        add_option('iok_tracking_redeem', json_encode($redeem_request), '', 'yes');
        custom_registration_shortcode();
    }
}

if (!function_exists('iok_deactivation_hook')) {
    function iok_deactivation_hook()
    {
        global $wpdb;

        $sql = "DROP TABLE IF EXISTS wp_iok_tracking";

        $wpdb->query($sql);
    }
}

if (!function_exists('iok_uninstall_hook')) {
    function iok_uninstall_hook()
    {
        global $wpdb;

        $sql = "DROP TABLE IF EXISTS wp_iok_tracking";

        $wpdb->query($sql);
    }
}

//register activation hook
register_activation_hook(__FILE__, 'iok_activation_hook');

//register deactivation hook
register_deactivation_hook(__FILE__, 'iok_deactivation_hook');

//register uninstall hook
register_uninstall_hook(__FILE__, 'iok_uninstall_hook');

//add menu in admin dashboard
add_action('admin_menu', 'iok_admin_menu_hook');

//admin menu hook callback
function iok_admin_menu_hook()
{
    add_menu_page('Iok Time Tracker', 'Daily Tracker', 'manage_options', 'ioktimetracker', 'iok_renderAdminUI');
    //add summary menu
    // add_submenu_page('ioktimetracker', 'Summary', 'Summary', 'manage_options', 'ioktracker_summary', 'iok_renderAdminSummary');
    //add redeem menu

    add_submenu_page('ioktimetracker', 'Reader', 'Reader', 'manage_options', 'ioktracker_reader', 'iok_renderAdminReader');

    add_submenu_page('ioktimetracker', 'Redeem', 'Redeem', 'manage_options', 'ioktracker_redeem', 'iok_renderAdminRedeem');

    add_submenu_page('ioktimetracker', 'Setting', 'Setting', 'manage_options', 'ioktracker_setting', 'iok_renderAdminSetting');
}

function __renderTable($dataSet1, $dataSet2, $max, $key, $title, $headers, $common)
{
    echo '<div class="welcome-panel">';
    echo '<h4>' . $title . ' (in mins.)</h4>';
    echo '<table width="60%">';

    echo '<tr>';

    foreach ($headers as $header) {
        echo '<th align="left">' . $header . '</th>';
    }

    echo '</tr>';

    for ($i = 0; $i < $max; $i++) { ?>
        <tr>
            <td>
                <?php
                if ($common == 'today') {
                    echo date('d-m-Y');
                } else if ($common == 'month') {
                    echo $i + 1;
                } else {
                    echo count($dataSet1) >= count($dataSet2) ? $dataSet1[$i]->year : $dataSet2[$i]->year;
                }
                ?>
            </td>
            <td>
                <?php
                echo (array_key_exists($i, $dataSet1) && property_exists($dataSet1[$i], $key) && $dataSet1[$i]->$key != null) ? $dataSet1[$i]->$key : '0' ?>
            </td>
            <td>
                <?php
                echo (array_key_exists($i, $dataSet2) && property_exists($dataSet2[$i], $key) && $dataSet2[$i]->$key != null) ? $dataSet2[$i]->$key : '0' ?>
            </td>
        </tr>
    <?php }
    echo '</table>';
    echo '</div>';
}

function iok_renderAdminReaderProfile($user_id)
{
    $user = get_userdata($user_id);
    $avatar = get_avatar_url($user_id);

    echo '<div class="welcome-panel">';
    echo "<h1>Iokonic Profile</h1>";
    echo '</div>';
    ?>

    <table>
        <tr>
            <td><img src="<?php echo $avatar ?>" alt="" width="80px"></td>
            <td>
                <h3>
                    <?php
                    echo $user->first_name ?> <?php
                    echo $user->last_name ?>
                    <p><?php
                        echo $user->user_email ?></p>
                    <p><?php
                        echo implode(', ', $user->roles) ?></p>
                </h3>
            </td>
        </tr>
    </table>

    <?php
    global $wpdb;

    $sqlTodayPost = "select round((sum(time_spent * time_interval) / 60),2) as total, date(created_at) as today from
                            wp_iok_tracking where user_id = $user_id and day(created_at) = day(now()) and post_id is not null";

    $sqlTodayPage = "select round((sum(time_spent * time_interval) / 60),2) as total from
                            wp_iok_tracking where user_id = $user_id and day(created_at) = day(now()) and post_id is null";

    $sqlMonthPost = "select round((sum(time_spent * time_interval) / 60),2) as total, month(created_at)
                            as month from wp_iok_tracking where user_id = $user_id group by month and post_id is not null";

    $sqlMonthPage = "select round((sum(time_spent * time_interval) / 60),2) as total, month(created_at)
                            as month from wp_iok_tracking where user_id = $user_id group by month and post_id is null";

    $sqlYearPost = "select round((sum(time_spent * time_interval) / 60),2) as total, year(created_at)
                        as year from wp_iok_tracking where user_id = $user_id  and post_id is not null group by year";

    $sqlYearPage = "select round((sum(time_spent * time_interval) / 60),2) as total, year(created_at)
                        as year from wp_iok_tracking where user_id = $user_id and post_id is null group by year";

    $resultTodayPost = $wpdb->get_results($sqlTodayPost);
    $resultTodayPage = $wpdb->get_results($sqlTodayPage);

    __renderTable($resultTodayPost, $resultTodayPage, 1, 'total', 'Today', ['Period', 'Post', 'Page'], 'today');

    $resultMonthPost = $wpdb->get_results($sqlMonthPost);
    $resultMonthPage = $wpdb->get_results($sqlMonthPage);

    __renderTable($resultMonthPost, $resultMonthPage, 12, 'total', 'Mothly', ['Month', 'Post', 'Page'], 'month');

    $resultYearPost = $wpdb->get_results($sqlYearPost);
    $resultYearPage = $wpdb->get_results($sqlYearPage);

    $max = count($resultYearPost) >= count($resultYearPage) ? count($resultYearPost) : count($resultYearPage);

    __renderTable($resultYearPost, $resultYearPage, $max, 'total', 'Yearly', ['Year', 'Post', 'Page'], 'year');
}

function iok_readerAdminReaders()
{
    global $wpdb;

    $sqlReaders = "select * from wp_users";

    $results = $wpdb->get_results($sqlReaders);

    echo '<div class="welcome-panel">';
    echo "<h1>Iokonic Readers</h1>";
    echo '<p>' . 'Phone number is required to enable account' . '</p>';
    echo '</div>';

    echo '<table style="width:60%">';
    echo '<tr>';

    echo '<th align="left">' . 'ID' . '</th>';
    echo '<th align="left">' . 'Login name' . '</th>';
    echo '<th align="left">' . 'Name' . '</th>';
    echo '<th align="left">' . 'Phone' . '</th>';
    echo '<th align="left">' . 'Account status' . '</th>';
    echo '<th align="left">' . 'Action' . '</th>';

    echo '</tr>';

    foreach ($results as $result) {
        echo '<tr>';
        echo '<td>' . $result->ID . '</td>';
        echo '<td>' . $result->user_nicename . '</td>';
        echo '<td>' . $result->user_nicename . '</td>';

        $phone = get_user_meta($result->ID, 'iok_phone', true);
        $status = get_user_meta($result->ID, 'iok_is_active', true);

        if (!empty($phone)) {
            echo '<td>' . $phone . '</td>';

            if ($status == 'yes') {
                echo '<td>' . 'Enable' . '</td>';
            } else {
                echo '<td>' . 'Disabled' . '</td>';
            }

        } else {
            echo '<td>' . '-' . '</td>';
            echo '<td>' . 'Disabled' . '</td>';
        }

        if ($status == 'yes') {
            echo '<td>' . '<form action="' . admin_url('admin-post.php') . '">' .
                '<input type="hidden" name="action" value="iok_update_account_status">' .
                '<input type="hidden" value="no" name="iok_is_active">' .
                '<input type="hidden" name="user_id" value="' . $result->ID . '">' .
                '<button class="button" type="submit">Disable</button>' .
                '</form></td>';
        } else {
            echo '<td>' . '<form action="' . admin_url('admin-post.php') . '">' .
                '<input type="hidden" name="action" value="iok_update_account_status">' .
                '<input type="hidden" value="yes" name="iok_is_active">' .
                '<input type="hidden" name="user_id" value="' . $result->ID . '">' .
                '<button class="button" type="submit">Enable</button>' .
                '</form></td>';
        }

        echo '</tr>';
    }

    echo '</table>';
}

function iok_renderAdminReader()
{
    if (!empty($_GET['user_id'])) {
        iok_renderAdminReaderProfile($_GET['user_id']);
    } else {
        iok_readerAdminReaders();
    }
}

function iok_renderAdminSetting()
{
    echo '<div class="welcome-panel">';
    echo "<h1>Iokonic Tracker Setting</h1>";
    echo '</div>';

    echo '<p class="">' .
        '<form action="' . admin_url('admin-post.php') . '">' .
        'Time interval (sec)' .
        '<input type="hidden" value="iok_update_config" name="action">' .
        '<input type="text" value="' . get_option('iok_tracking_interval') . '" name="interval" style="width:40px; height:28px">' .
        ' Per page ' .
        '<input type="text" value="' . get_option('iok_tracking_per_page') . '" name="per_page" style="width:40px; height:28px">' .
        ' Track by' .
        ' <input type="radio" name="track_by" value="post"> Post' .
        ' <input type="radio" name="track_by" value="any"> Anywhere' .
        ' <button class="button" type="submit"> Update </button>' .
        '</form>' .
        '</p>';

    echo '<p class="">' .
        '<form action="' . admin_url('admin-post.php') . '">' .
        'Information Url ' .
        '<input type="hidden" value="iok_update_config" name="action">' .
        '<input type="text" value="' . get_option('iok_tracking_url') . '" name="iok_tracking_url" >' .
        ' <button class="button" type="submit"> Update </button>' .
        '</form>' .
        '</p>';

    echo '<p class="">' .
        '<form action="' . admin_url('admin-post.php') . '">' .
        'Time limit (min) ' .
        '<input type="hidden" value="iok_update_config" name="action">' .
        '<input type="text" value="' . get_option('iok_tracking_timelimit') . '" name="iok_tracking_timelimit" >' .
        ' <button class="button" type="submit"> Update </button>' .
        '</form>' .
        '</p>';

    echo '<p class="">' .
        '<form action="' . admin_url('admin-post.php') . '">' .
        'Minimum Payment (Rs.) ' .
        '<input type="hidden" value="iok_update_config" name="action">' .
        '<input type="text" value="' . get_option('iok_tracking_minimumPayment') . '" name="iok_tracking_minimumPayment" >' .
        ' <button class="button" type="submit"> Update </button>' .
        '</form>' .
        '</p>';

    echo '<p class="">' .
        '<form action="' . admin_url('admin-post.php') . '">' .
        'Registered Phone ' .
        '<input type="hidden" value="iok_update_config" name="action">' .
        '<input type="text" value="' . get_option('iok_registered_phone') . '" name="iok_registered_phone" >' .
        ' <button class="button" type="submit"> Update </button>' .
        '</form>' .
        '</p>';

}

function iok_renderAdminRedeem()
{

    global $wpdb;

    $sql = "select * from wp_users inner join wp_usermeta on wp_usermeta.user_id = wp_users.ID where wp_usermeta.meta_key = 'redeems' ";

    $results = $wpdb->get_results($sql);

    echo '<div class="welcome-panel">';
    echo "<h1>Redeems</h1>";
    echo '</div>';

    echo '<table style="width:100%">';
    echo '<tr>';

    echo '<th align="left">' . 'User' . '</th>';
    echo '<th align="left">' . 'Redeem Request' . '</th>';

    echo '</tr>';

    foreach ($results as $result) {
        $sql = "select * from wp_users inner join wp_usermeta on wp_usermeta.user_id = wp_users.ID where wp_usermeta.meta_key = 'iok_phone' ";

        $phone = $wpdb->get_results($sql);

        echo '<tr>';
        echo '<td>' . $result->ID . ' ' . $result->user_login . '<br>';
        if(count($phone) > 0) {
            echo '(' . $phone[0]->iok_phone . ')';
        }else {
            echo '(-)';
        }

        echo 'Current total payable: Rs. <strong>' . get_payable_by_id($result->ID) . '</strong>' .
            '</td>';

        $redeems = json_decode($result->meta_value);
        echo '<td>';
        echo '<table>';

        echo '<tr>';
        echo '<th align="left">' . 'Date' . '</th>';
        echo '<th align="left">' . 'Amount payable ' . '</th>';
        echo '<th align="left">' . 'Paid' . '</th>';
        echo '<th align="left">' . 'Action' . '</th>';
        echo '</tr>';

        foreach ($redeems as $redeem) {
            echo '<tr>';
            echo '<td>' . $redeem->date . '</td>';
            echo '<td><strong>' . 'Rs. ~/-' . '</strong></td>';
            echo '<td>' . $redeem->paid . '</td>';
            echo '<td>' . '<form action="' . admin_url('admin-post.php') . '">' .
                '<input type="hidden" name="action" value="iok_process_redeems">' .
                '<input type="hidden" value="' . $redeem->date . '" name="date">' .
                '<input type="hidden" name="user_id" value="' . $result->ID . '">';
            if ($redeem->paid == 'yes') {
                echo '<button class="button" type="submit" disabled>Pay</button>';
            } else {
                echo '<button class="button" type="submit">Pay</button>';
            }
            echo '</form></td>';
            echo '</tr>';
        }

        echo '</table>';
        echo '</td>';

        echo '</tr>';
    }

    echo '</table>';
}

//summary
function iok_renderAdminSummary()
{
    echo '<div class="welcome-panel">';
    echo "<h1>Iokonic Tracking Summary</h1>";
    echo '<p class="">Today total: <strong>' . $total . ' min</strong>
    | Post read: <strong>' . $post . 'min</strong>
    | Page read: <strong>' . $page . ' min </strong></p>';
    echo '</div>'; ?>
    <p>
    <form action="">
        Day <select name="day" id="">
            <?php foreach (range(1, 31) as $day) {
                echo "<option value=$day>$day</option>";
            } ?>
        </select>
        Month<select name="month" id="">
            <?php foreach (range(1, 12) as $month) {
                echo "<option value=$month>$month</option>";
            } ?>
        </select>
        Year<select name="year" id="">
            <?php foreach (range(2019, 2025) as $year) {
                echo "<option value=$year>$year</option>";
            } ?>
        </select>
        <input type="submit" value="Summary" class="button">
    </form>
    </p>
    <?php

    global $wpdb;

    $limit = empty($_REQUEST['per_page']) ? 10 : (int)($_REQUEST['per_page']);
    $offset = empty($_REQUEST['page']) ? 1 : (int)($_REQUEST['page']);

    $sqlPost = "select wp_iok_tracking.*,wp_users.user_nicename,
                wp_posts.post_title from wp_iok_tracking
                inner join wp_users on wp_users.ID = wp_iok_tracking.user_id
                inner join wp_posts on wp_posts.ID = wp_iok_tracking.post_id
                order by wp_iok_tracking.created_at desc";

    $sqlPage = "select wp_iok_tracking.*,wp_users.user_nicename
                        from wp_iok_tracking inner join wp_users on
                        wp_users.ID = wp_iok_tracking.user_id
                        where wp_iok_tracking.post_id is null
                       order by wp_iok_tracking.created_at desc";

    $resultPost = $wpdb->get_results($sqlPost);
    $resultPage = $wpdb->get_results($sqlPage);
    var_dump($resultPost);
    var_dump($resultPage);
}

function iok_renderAdminTitle()
{
    global $wpdb;

    $sql = "select sum(time_spent * time_interval) / 60 as post_read from wp_iok_tracking
    where post_id is not null and day(created_at) = day(now())
    union
    select sum(time_spent * time_interval) / 60 as page_read from wp_iok_tracking
    where page_name is not null and day(created_at) = day(now())";

    $results = $wpdb->get_results($sql);

    $total = $page = $post = 0;

    if (count($results) == 2) {
        $post = floatval($results[0]->post_read);
        $page = floatval($results[1]->post_read);

        $total = $post + $page;
    }

    echo '<div class="welcome-panel">';
    echo "<h1>Iokonic Time tracking</h1>";
    echo '<p class="">Today total: <strong>' . $total . ' min</strong>
    | Post read: <strong>' . $post . 'min</strong>
    | Page read: <strong>' . $page . ' min </strong></p>';
    echo '</div>';
}

//render ui when admin click on admin menu
function iok_renderAdminUI()
{
    iok_renderAdminTitle();

    global $wpdb;

    $limit = empty($_REQUEST['per_page']) ? 10 : (int)($_REQUEST['per_page']);
    $offset = empty($_REQUEST['page']) ? 1 : (int)($_REQUEST['page']);

    $sql = "select wp_iok_tracking.*,wp_users.user_nicename,wp_users.ID as user_id,
    wp_posts.post_title
     from wp_iok_tracking inner join wp_users on wp_users.ID = wp_iok_tracking.user_id
                inner join wp_posts on wp_posts.ID = wp_iok_tracking.post_id
                where day(created_at) = day(now()) and page_name is null
                order by created_at desc";

    $results = $wpdb->get_results($sql);

    echo '<div class="welcome-panel">';
    echo "<h1>Post Tracking</h1>";
    echo '<p> Today </p>';
    echo '</div>';

    echo '<table style="width:60%">';

    echo '<tr>';

    echo '<th align="left">' . '#' . '</th>';
    echo '<th align="left">' . 'Name' . '</th>';
    echo '<th align="left">' . 'Post' . '</th>';
    echo '<th align="left">' . 'Mins' . '</th>';
    echo '<th align="left">' . 'Paid' . '</th>';
    echo '<th align="left">' . 'Last read at' . '</th>';

    echo '</tr>';

    foreach ($results as $result) {
        echo '<tr>';
        echo '<td>' . $result->id . '</td>';
        echo '<td>' . '<form action="' . admin_url('admin.php') . '">' .
            '<input type="hidden" name="page" value="ioktracker_reader">' .
            '<input type="hidden" name="user_id" value="' . $result->user_id . '">' .
            '<input type="submit" value="' . $result->user_nicename . '" style="border: none;background-color: #f1f1f1;text-decoration: underline;">' .
            '</form></td>';
        echo '<td>' . $result->post_title . '</td>';
        echo '<td>' . $result->time_spent . '</td>';
        echo '<td>' . $result->paid . '</td>';
        echo '<td>' . $result->created_at;
        echo '<td>' .
            '<form action="' . admin_url('admin-post.php') . '">' .
            "<input type=\"hidden\" name=\"action\" value=\"update_paid\">" .
            "<input type=\"hidden\" name=\"id\" value=$result->id>" .
            '<button class="button" type="submit"> Paid </button>' .
            '</form>' .
            '</td>';
        echo '</form>';
        echo '</tr>';
    }

    echo '</table>';

    $limit = empty($_REQUEST['per_page']) ? 10 : (int)($_REQUEST['per_page']);
    $offset = empty($_REQUEST['page']) ? 1 : (int)($_REQUEST['page']);

    $sql = "select wp_iok_tracking.*,wp_users.user_nicename, wp_users.ID as user_id
                from wp_iok_tracking inner join wp_users on wp_users.ID = wp_iok_tracking.user_id
                where day(created_at) = day(now()) and page_name is not null
                order by created_at desc";

    $results = $wpdb->get_results($sql);

    echo '<div class="welcome-panel">';
    echo "<h1>Page Tracking</h1>";
    echo '<p> Today </p>';
    echo '</div>';

    echo '<table style="width:60%">';

    echo '<tr>';

    echo '<th align="left">' . '#' . '</th>';
    echo '<th align="left">' . 'Name' . '</th>';
    echo '<th align="left">' . 'Page' . '</th>';
    echo '<th align="left">' . 'Mins' . '</th>';
    echo '<th align="left">' . 'Paid' . '</th>';
    echo '<th align="left">' . 'Last read at' . '</th>';

    echo '</tr>';

    foreach ($results as $result) {
        echo '<tr>';
        echo '<td>' . $result->id . '</td>';
        echo '<td>' . '<form action="' . admin_url('admin.php') . '">' .
            '<input type="hidden" name="page" value="ioktracker_reader">' .
            '<input type="hidden" name="user_id" value="' . $result->user_id . '">' .
            '<input type="submit" value="' . $result->user_nicename . '" style="border: none;background-color: #f1f1f1;text-decoration: underline;">' .
            '</form></td>';
        echo '<td>' . $result->page_name . '</td>';
        echo '<td>' . $result->time_spent . '</td>';
        echo '<td>' . $result->paid . '</td>';
        echo '<td>' . $result->created_at . '</td>';
        echo '<td>' .
            '<form action="' . admin_url('admin-post.php') . '">' .
            "<input type=\"hidden\" name=\"action\" value=\"update_paid\">" .
            "<input type=\"hidden\" name=\"id\" value=$result->id>" .
            '<button class="button" type="submit"> Paid </button>' .
            '</form>' .
            '</td>';
        echo '</form>';
        echo '</tr>';
    }
    echo '</table>';
}

//add hook for ajax handler
add_action('wp_ajax_nopriv_track_time', 'track_time');
add_action('wp_ajax_track_time', 'track_time');

add_action('wp_ajax_nopriv_iok_redeem_request', 'iok_redeem_request');
add_action('wp_ajax_iok_redeem_request', 'iok_redeem_request');

//send redeem requests
function iok_redeem_request()
{
    if (is_user_logged_in()) {

        $redeemRequests = get_user_meta(get_current_user_id(), 'redeems', true);

        if (empty($redeemRequests)) {

            $time = get_payable_time();

            if ($time < 100) {
                echo json_encode(['message' => 'You do not have enough payable time']);
                die;
            }

            $r = new stdClass();
            $r->date = date('d-m-Y H:i:s');
            $r->paid = 'no';

            $redeem[] = $r;

            update_user_meta(get_current_user_id(), 'redeems', json_encode($redeem));

            echo json_encode(['message' => 'Your redeem request has been received']);
            die;
        } else {

            $redeems = json_decode($redeemRequests);

            $unpaidRedeems = array_filter($redeems, function ($redeem) {
                return $redeem->paid == 'no';
            });

            if (count($unpaidRedeems)) {
                echo json_encode(['message' => 'You already have unpaid redeems, please contact admin.']);
                die;
            } else {
                $time = get_payable_time();

                if ($time < 100) {
                    echo json_encode(['message' => 'You do not have enough payable time']);
                    die;
                }

                $r = new stdClass();
                $r->date = date('d-m-Y H:i:s');
                $r->paid = 'no';

                array_push($redeems, $r);

                update_user_meta(get_current_user_id(), 'redeems', json_encode($redeems));
                echo json_encode(['message' => 'Your redeem request has been received']);
                die;
            }
        }
    } else {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }
}

//ajax hook ajax callback
function track_time()
{
    global $wpdb;

    if (is_user_logged_in()) {
        $data = 0;

        $is_post = (bool)$_GET['is_post'];

        $user_id = get_current_user_id();
        $post_id = $_GET['post_id'];
        $interval = 30;
        $page_name = $_GET['page_url'];

        if ($_GET['is_post'] == 'true') {
            //user is reading post

            $sql = "select * from wp_iok_tracking where
            user_id = $user_id and post_id = $post_id
            and time_interval = $interval and
            day(created_at) = day(now()) and paid = 'no' ";

            $result = $wpdb->get_results($sql);

            if (count($result) == 0) {
                //this is first time user is visiting this post,
                $status = $wpdb->insert('wp_iok_tracking', [
                    'user_id' => $user_id,
                    'post_id' => $post_id,
                    'time_spent' => 1,
                    'time_interval' => $interval,
                ]);

                // echo json_encode(['status' => $status, 'total_read' => get_todays_total_time()]);
                // exit;
            } else {
                $sql = "update wp_iok_tracking set time_spent = time_spent +1
                    where user_id = $user_id and post_id = $post_id and time_interval = $interval";

                $wpdb->get_results($sql);
            }

            // echo json_encode(['message' => count($result)]);

        } else {

            $sql = "select * from wp_iok_tracking where user_id = $user_id
             and page_name = '$page_name'
             and time_interval = $interval
             and day(created_at) = day(now())";

            $result = $wpdb->get_results($sql);

            if (count($result) == 0) {
                //this is first time user is visiting this post,
                $status = $wpdb->insert('wp_iok_tracking', [
                    'user_id' => $user_id,
                    'page_name' => $page_name,
                    'time_spent' => 1,
                    'time_interval' => $interval,
                ]);

                // echo json_encode(['status' => $status]);
                // exit;
            } else {
                $sql = "update wp_iok_tracking set time_spent = time_spent +1
                    where user_id = $user_id and page_name = '$page_name' and time_interval = $interval";

                $wpdb->get_results($sql);
            }

            //user is reading page
            // echo json_encode(['message' => 'You are reading a page']);
        }

        $total = 0;
        $t = get_todays_total_time();

        if (count($t) == 1) {
            $t = number_format((float)$t[0]->total_time, 2, '.', '');
            $total = $t;
        }

        echo json_encode(['total_time' => $total]);
        exit;
    }
    echo json_encode(['message' => 'Unauthenticated']);
    exit;
}

add_action('admin_post_update_paid', 'update_paid');

add_action('admin_post_iok_update_config', 'iok_update_config');

add_action('admin_post_iok_track_profile', 'io_track_profile');

add_action('admin_post_iok_update_account_status', 'iok_update_account_status');

add_action('admin_post_iok_process_redeems', 'iok_process_redeems');

//process redeem request from the user
function iok_process_redeems()
{
    if (!empty($_GET['user_id']) && !empty($_GET['date'])) {
        global $wpdb;

        $user_id = $_GET['user_id'];
//
//        $sql = "select id, (time_interval * time_spent)/60 as time, post_id
//                  from wp_iok_tracking where user_id = $user_id
//                  and paid = 'no' and post_id is not null";

//        $results = $wpdb->get_results($sql);
//
//        $sum = 100;
//
//        $ids = [];
//
//        $t = 0;
//
//        foreach ($results as $result) {
//            $t += $result->time;
//            if ($t == $sum) {
//                $ids[] = $$result->id;
//                break;
//            }
//
//            if ($t > $sum) {
//                $d = $t - $sum; // paid = no, the diff is in min
//                // ($d * 60) / 30 for interval
//                $p = $result->time - $d;
//
//                $p = $p * 2;
//
//                $q = "update wp_iok_tracking set time_spent = $p  and paid = 'yes' where id = $result->id";
//
//                $wpdb->get_results($q);
//                //create a new reading time span
//                $wpdb->insert('wp_iok_tracking', [
//                    'time_spent' => $d * 2,
//                    'user_id' => $user_id,
//                    'time_interval' => 30,
//                    'post_id' => $result->post_id,
//                    'paid' => 'no'
//                ]);
//                break;
//            }
//
//            $ids[] = $result->id;
//        }

//        count($ids);
//
//        $ids = implode(',', array_map('absint', $ids));
//
        $wpdb->query("update wp_iok_tracking set paid = 'yes' WHERE post_id is not null and user_id = $user_id and user_id is not null");
//
        //update user meta
        $redeems = get_user_meta($_GET['user_id'], 'redeems', true);
        $redeems = json_decode($redeems);

        foreach ($redeems as $redeem) {
            if ($redeem->date == $_GET['date']) {
                $redeem->paid = 'yes';
                break;
            }
        }

        update_user_meta($_GET['user_id'], 'redeems', json_encode($redeems));
    }
    //redirect back
    wp_redirect($_SERVER['HTTP_REFERER']);
}

function iok_update_account_status()
{
    if (!empty($_GET['iok_is_active']) && !empty($_GET['user_id'])) {
        update_user_meta($_GET['user_id'], 'iok_is_active', $_GET['iok_is_active']);
    }

    wp_redirect($_SERVER['HTTP_REFERER']);
}

function iok_update_config()
{
    if (intval($_GET['interval']) >= 10) {
        update_option('iok_tracking_interval', intval($_GET['interval']));
        var_dump($_SERVER);
    }

    if (intval($_GET['per_page']) > 0) {
        update_option('iok_tracking_per_page', intval($_GET['per_page']));
    }

    if (in_array(($_GET['track_by']), ['post', 'any'])) {
        update_option('iok_tracking_track_by', $_GET['track_by']);
    }

    if (!empty($_GET['iok_tracking_url'])) {
        update_option('iok_tracking_url', $_GET['iok_tracking_url']);
    }

    if (!empty($_GET['iok_tracking_timelimit'])) {
        update_option('iok_tracking_timelimit', $_GET['iok_tracking_timelimit']);
    }

    if (!empty($_GET['iok_tracking_minimumPayment']) && is_numeric($_GET['iok_tracking_minimumPayment'])) {
        update_option('iok_tracking_minimumPayment', $_GET['iok_tracking_minimumPayment']);
    }

    if (!empty($_GET['iok_registered_phone']) && is_numeric($_GET['iok_registered_phone'])) {
        update_option('iok_registered_phone', $_GET['iok_registered_phone']);
    }

    wp_redirect($_SERVER['HTTP_REFERER']);
}

function update_paid()
{
    $id = intval($_GET['id']);

    global $wpdb;

    $status = $wpdb->update('wp_iok_tracking', ['paid' => 'yes'], ['id' => $id]);

    wp_redirect($_SERVER['HTTP_REFERER']);
}

//debug
function add_into_header()
{
    global $wp;
    global $wp_query;

    $to_json = [];

    $to_json['is_logged_in'] = is_user_logged_in();
    $to_json['is_post'] = false;
    $to_json['post_id'] = null;
    $to_json['page_url'] = null;
    $to_json['info_url'] = get_option('iok_tracking_url');
    $to_json['interval'] = get_option('iok_tracking_interval');

    $to_json['is_active'] = get_user_meta(get_current_user_id(), 'is_active') != 'yes' ? false : true;

    if (is_singular()) {
        $to_json['is_post'] = true;
        $to_json['post_id'] = $wp_query->post->ID;
    } else {
        $to_json['page_url'] = home_url(add_query_arg(array()), $wp->request);
    }

    echo '<script>';
    echo 'var iok_tracking=' . json_encode($to_json);
    echo '</script>';

    if (is_user_logged_in()) {
        $time = get_payable_time();

        $userPhone = get_user_meta(get_current_user_id(), 'iok_phone', true);
        $isActive = get_user_meta(get_current_user_id(), 'iok_is_active', true);

        if (empty($userPhone)) {
            add_user_meta(get_current_user_id(), 'iok_phone', null);
        }

        if (empty($isActive)) {
            add_user_meta(get_current_user_id(), 'iok_is_active', 'no');
        }

        if (isset($_REQUEST['phone']) && !empty($_REQUEST['phone'])) {
            update_user_meta(get_current_user_id(), 'iok_phone', $_REQUEST['phone']);
            unset($_REQUEST['phone']);
        }

        $userPhone = get_user_meta(get_current_user_id(), 'iok_phone', true);
        $isActive = get_user_meta(get_current_user_id(), 'iok_is_active', true);

        ?>
        <div id="iok_header"
             style="display:flex; justify-content:center; background-color:#f44336; padding: 0.2 rem 0.1 rem; color:white !important">
            <?php
            if ($isActive == 'no') {
                ?>
                <?php if ($userPhone != null || !empty($userPhone)) { ?>
                    <div>
                        Since account is not activated your reading time will <strong>not</strong> be
                        <strong>recorded</strong>.
                        To activate your account,
                        pay Rs. <?php echo get_option('iok_tracking_minimumPayment'); ?>
                        to <?php echo get_option('iok_registered_phone'); ?> and wait for admin verification.
                    </div>
                <? } else { ?>
                    <div>
                        Since your account is not activated,
                        your reading time will not be <strong>recorded</strong>.
                        To activate your account,
                        provide your Paytm phone
                        <form action="<?php
                        echo $_SERVER['REQUEST_URI'] ?>" method="post">
                            <input type="text" name="phone">
                            <input type="hidden" name="action" value="iok_update_phone">
                            <input type="submit" value="Update Phone" name="UpdatePhone">
                            and pay Rs. <?php
                            echo get_option('iok_tracking_minimumPayment'); ?>
                            to <?php
                            echo get_option('iok_registered_phone'); ?> and wait for admin verification.
                        </form>
                        <span><a href="update_user_profile">Profile</a></span>
                    </div>
                    <?php
                }
            } else { ?>
            <div id="iok_redeem_panel">
                You earn Rs. <?php echo get_payable_money(); ?> so far.
                You can redeem after <?
                echo get_option('iok_tracking_timelimit'); ?> mins.
            </div>
            <div>
                <?php if ($time < get_option('iok_tracking_timelimit')) { ?>
                    <button onclick='iokHidePanel()'>OK</button>
                <?php } else { ?>
                    <img src="<?
                    //					echo plugin_dir_url( __FILE__ ) . '/images/loader.gif'; ?><!--" alt=""
                         height="48px" width="48px">
                    <button value="redeem" name="redeem" id="btn_request_redeem">Redeem</button>
                <?php }
                } ?>
            </div>

        </div>
        <script>
            // var iok_panel
            function iokHidePanel() {
                document.getElementById('iok_header').style.display = "none"
            }
        </script>
    <?php }
}


add_action('wp_head', 'add_into_header');

//Compute today's total time spend by the user accumulativly

function get_todays_total_time()
{
    global $wpdb;

    $user_id = get_current_user_id();

    $sql = "select sum(time_spent * time_interval) / 60  as total_time from
     wp_iok_tracking where user_id = $user_id and day(created_at) = day(now())";

    return $wpdb->get_results($sql);
}

//Compute today's total time spend by the user accumulatively

function get_payable_time()
{
    global $wpdb;

    $user_id = get_current_user_id();

    $sql = "select sum(time_spent * time_interval) / 60  as total from
     wp_iok_tracking where user_id = $user_id and paid = 'no' and post_id is not null";

    $results = $wpdb->get_results($sql)[0]->total;

    return is_null($results) ? 0 : floatval($results);
}

function get_payable_by_id($user_id)
{
    global $wpdb;

    $sql = "select sum(time_spent * time_interval) / 60  as total from
     wp_iok_tracking where user_id = $user_id and paid = 'no' and post_id is not null";

    $results = $wpdb->get_results($sql)[0]->total;

    return is_null($results) ? 0 : intval($results) / 10;
}

function get_payable_money()
{
    global $wpdb;

    $user_id = get_current_user_id();

    $sql = "select sum(time_spent * time_interval) / 60  as total from
     wp_iok_tracking where user_id = $user_id and paid = 'no' and post_id is not null";

    $results = $wpdb->get_results($sql)[0]->total;

    return is_null($results) ? 0 : (int)(floatval($results) / 10);
}

add_action('admin_post_rr', 'iok_request_redeem');

add_action('admin_post_iok_user_profile', 'iok_user_profile');

add_action('admin_post_nopriv_iok_user_profile', 'iok_user_profile');

add_action('admin_post_show_register_page', 'show_register_page');

add_action('admin_post_iok_update_phone', 'iok_update_phone');

function iok_user_profile()
{

//    var_dump($_REQUEST);
//
//    die;

    if (get_current_user_id() == 0) {
        wp_redirect($_SERVER['HTTP_REFERER']);
    }

    get_header();

    if (isset($_GET['redeem'])) {

        $redeemRequests = get_user_meta(get_current_user_id(), 'redeems', true);

        if (empty($redeemRequests)) {
            $r = new stdClass();
            $r->date = date('d-m-Y H:i:s');
            $r->paid = 'no';

            $redeem[] = $r;

            update_user_meta(get_current_user_id(), 'redeems', json_encode($redeem));

            echo '<div>Your redeem request has been sent.</div>';
        } else {

            $redeems = json_decode($redeemRequests);

            $unpaidRedeems = array_filter($redeems, function ($redeem) {
                return $redeem->paid == 'no';
            });

            if (count($unpaidRedeems)) {
                echo '<div>You already have unpaid redeems, please contact admin.</div>';
            } else {

                $r = new stdClass();
                $r->date = date('d-m-Y H:i:s');
                $r->paid = 'no';

                array_push($redeems, $r);

                update_user_meta(get_current_user_id(), 'redeems', json_encode($redeems));
                echo '<div>Your redeem request has been sent.</div>';
            }
        }
    }

    $redeems = json_decode(get_user_meta(get_current_user_id(), 'redeems', true));

    echo '<h4> Your redeem history</h4>';

    if (count($redeems)) {
        echo '<table class="table">';
        echo '<tr>';
        echo '<th>Date</th>';
        echo '<th>Paid</th>';
        echo '</tr>';

        foreach ($redeems as $redeem) {
            echo '<tr>';
            echo '<td align="center">' . $redeem->date . '</td>';
            echo '<td align="center">Rs. ' . $redeem->paid . '</td>';
            echo '</tr>';
        }

        echo '</table>';
    }

    get_footer();
}

function process_redeem_request()
{
    $sql = " select sum(time_spent * time_interval) / 60 as total
    from wp_iok_tracking where paid ='no'";
}

////////////////////Form///////////////////
function registration_form($username, $password, $email, $website, $first_name, $last_name, $nickname, $bio)
{
    echo '
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
    <table>
    <tr>
        <td><label for="username">Username <strong>*</strong></label></td>
        <td><input type="text" name="username" value="' . (isset($_POST['username']) ? $username : null) . '"></td>
    </tr>

    <tr>
    <td>    <label for="password">Password <strong>*</strong></label></td>
    <td>    <input type="password" name="password" value="' . (isset($_POST['password']) ? $password : null) . '"></td>
    </tr>

    <tr>
    <td>    <label for="email">Email <strong>*</strong></label></td>
    <td>    <input type="text" name="email" value="' . (isset($_POST['email']) ? $email : null) . '"></td>
    </tr>

    <tr>
    <td>    <label for="firstname">First Name</label></td>
    <td>    <input type="text" name="fname" value="' . (isset($_POST['fname']) ? $first_name : null) . '"></td>
    </tr>

    <tr>
    <td>    <label for="website">Last Name</label></td>
    <td>    <input type="text" name="lname" value="' . (isset($_POST['lname']) ? $last_name : null) . '"></td>
    </tr>

    <tr>
<td><label for="nickname">Nickname</label></td>
<td><input type="text" name="nickname" value="' . (isset($_POST['nickname']) ? $nickname : null) . '"></td>
    </tr>
<tr>
<td>    <input type="submit" name="submit" value="Register"/>
</td>
</tr>
    </table>
    </form>
    ';
}

function registration_validation($username, $password, $email, $website, $first_name, $last_name, $nickname, $bio)
{
    global $reg_errors;
    $reg_errors = new WP_Error;

    if (empty($username) || empty($password) || empty($email)) {
        $reg_errors->add('field', 'Required form field is missing');
    }

    if (4 > strlen($username)) {
        $reg_errors->add('username_length', 'Username too short. At least 4 characters is required');
    }
    if (username_exists($username)) {
        $reg_errors->add('user_name', 'Sorry, that username already exists!');
    }

    if (!validate_username($username)) {
        $reg_errors->add('username_invalid', 'Sorry, the username you entered is not valid');
    }

    if (5 > strlen($password)) {
        $reg_errors->add('password', 'Password length must be greater than 5');
    }

    if (!is_email($email)) {
        $reg_errors->add('email_invalid', 'Email is not valid');
    }

    if (email_exists($email)) {
        $reg_errors->add('email', 'Email Already in use');
    }

    if (!empty($website)) {
        if (!filter_var($website, FILTER_VALIDATE_URL)) {
            $reg_errors->add('website', 'Website is not a valid URL');
        }
    }

    if (is_wp_error($reg_errors)) {

        foreach ($reg_errors->get_error_messages() as $error) {

            echo '<div>';
            echo '<strong>ERROR</strong>:';
            echo $error . '<br/>';
            echo '</div>';

        }

    }
}

function complete_registration()
{
    global $reg_errors, $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
    if (1 > count($reg_errors->get_error_messages())) {
        $userdata = array(
            'user_login' => $username,
            'user_email' => $email,
            'user_pass' => $password,
            'user_url' => $website,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'nickname' => $nickname,
            'description' => $bio,
        );
        $user = wp_insert_user($userdata);
        echo 'Registration complete. Goto <a href="' . get_site_url() . '/wp-login.php">login page</a>.';
    }
}

function custom_registration_function()
{
    if (isset($_POST['submit'])) {
        registration_validation(
            $_POST['username'],
            $_POST['password'],
            $_POST['email'],
            $_POST['website'],
            $_POST['fname'],
            $_POST['lname'],
            $_POST['nickname'],
            $_POST['bio']
        );

        // sanitize user form input
        global $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
        $username = sanitize_user($_POST['username']);
        $password = esc_attr($_POST['password']);
        $email = sanitize_email($_POST['email']);
        $website = esc_url($_POST['website']);
        $first_name = sanitize_text_field($_POST['fname']);
        $last_name = sanitize_text_field($_POST['lname']);
        $nickname = sanitize_text_field($_POST['nickname']);
        $bio = esc_textarea($_POST['bio']);

        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
            $username,
            $password,
            $email,
            $website,
            $first_name,
            $last_name,
            $nickname,
            $bio
        );
    }

    registration_form(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
    );
}

add_shortcode('cr_custom_registration', 'custom_registration_shortcode');

// The callback function that will replace [book]
function custom_registration_shortcode()
{
    ob_start();
    custom_registration_function();

    return ob_get_clean();
}
